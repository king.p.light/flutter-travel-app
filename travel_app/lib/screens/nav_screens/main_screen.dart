import 'package:flutter/material.dart';
import 'package:travel_app/screens/nav_screens/bar_items_screen.dart';
import 'package:travel_app/screens/home_screen.dart';
import 'package:travel_app/screens/nav_screens/profile_screen.dart';
import 'package:travel_app/screens/nav_screens/search_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List pages = [
    HomeScreen(),
    BarItemsScreen(),
    SearchScreen(),
    ProfileScreen(),
  ];
  int currentIndex = 0;

  void onTapBottomNavigation(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTapBottomNavigation,
        currentIndex: currentIndex,
        elevation: 0.0,
        type: BottomNavigationBarType.shifting,
        showSelectedLabels: true,
        showUnselectedLabels: false,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.grey.withOpacity(0.6),
        backgroundColor: Colors.white,
        items: const [
          BottomNavigationBarItem(
            label: 'Home',
            icon: Icon(Icons.apps),
          ),
          BottomNavigationBarItem(
            label: 'Bar',
            icon: Icon(Icons.bar_chart_rounded),
          ),
          BottomNavigationBarItem(
            label: 'Search',
            icon: Icon(Icons.search_rounded),
          ),
          BottomNavigationBarItem(
            label: 'Profile',
            icon: Icon(Icons.person_outline_rounded),
          ),
        ],
      ),
      body: pages[currentIndex],
    );
  }
}
