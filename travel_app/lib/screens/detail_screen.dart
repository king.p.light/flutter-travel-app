import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_app/logic/cubit/app_cubit.dart';
import 'package:travel_app/misc/colors.dart';
import 'package:travel_app/widgets/app_buttons.dart';

import 'package:travel_app/widgets/app_large_text.dart';
import 'package:travel_app/widgets/app_text.dart';
import 'package:travel_app/widgets/responsive_button.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  int gottenStars = 3;
  int selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppCubit, AppState>(
      builder: (context, state) {
        AppDetailState detail = state as AppDetailState;
        return Scaffold(
          body: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            child: Stack(
              children: [
                // background
                Positioned(
                  left: 0.0,
                  right: 0.0,
                  child: Container(
                    width: double.maxFinite,
                    height: 350.0,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                          // "https://jsonplaceholder.typicode.com/photos/" +
                          detail.place.url,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),

                // menu icon
                Positioned(
                  left: 20.0,
                  top: 50.0,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          BlocProvider.of<AppCubit>(context).goHome();
                        },
                        icon: Icon(Icons.menu_rounded),
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),

                Positioned(
                  top: 320.0,
                  child: Container(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 30.0),
                    width: MediaQuery.of(context).size.width,
                    height: 500.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Place header text
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            AppLargeText(
                              text: 'Yosemite',
                              // text: detail.places.name,
                              color: Colors.black.withOpacity(0.8),
                            ),
                            AppLargeText(
                              // text: '\$ 250',
                              text: '\$ ' + detail.place.id.toString(),
                              color: AppColors.mainColor,
                            ),
                          ],
                        ),
                        SizedBox(height: 10.0),

                        // location icon
                        Row(
                          children: [
                            Icon(
                              Icons.location_on_rounded,
                              color: AppColors.mainColor,
                            ),
                            SizedBox(width: 10.0),
                            AppText(
                              text: 'USA, California',
                              // text: detail.places.location,
                              color: AppColors.textColor1,
                            ),
                          ],
                        ),
                        SizedBox(height: 20.0),

                        // star icon
                        Row(
                          children: [
                            Wrap(
                              children: List.generate(
                                5,
                                (index) {
                                  return Icon(
                                    Icons.star_rate_rounded,
                                    color:
                                        // index < gottenStars
                                        index < detail.place.albumId
                                            ? AppColors.starColor
                                            : AppColors.textColor2,
                                  );
                                },
                              ),
                            ),
                            SizedBox(width: 10.0),
                            AppText(text: '(5.0)', color: AppColors.textColor2),
                          ],
                        ),
                        SizedBox(height: 25.0),

                        // people text
                        AppLargeText(
                          text: 'People',
                          size: 20.0,
                          color: Colors.black.withOpacity(0.8),
                        ),
                        SizedBox(height: 5.0),

                        AppText(
                          text: 'Number of people in your group',
                          color: AppColors.mainTextColor,
                        ),
                        SizedBox(height: 10.0),

                        Wrap(
                          children: List.generate(
                            5,
                            (index) {
                              return InkWell(
                                onTap: () {
                                  setState(() {
                                    selectedIndex = index;
                                  });
                                },
                                child: Container(
                                  margin: const EdgeInsets.only(right: 10.0),
                                  child: AppButtons(
                                    text: (index + 1).toString(),
                                    size: 50.0,
                                    backgroundColor: selectedIndex == index
                                        ? Colors.black
                                        : AppColors.buttonBackground,
                                    borderColor: selectedIndex == index
                                        ? Colors.black
                                        : AppColors.buttonBackground,
                                    color: selectedIndex == index
                                        ? Colors.white
                                        : Colors.black,
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(height: 20.0),

                        // description text
                        AppLargeText(
                          text: 'Description',
                          size: 20.0,
                          color: Colors.black.withOpacity(0.8),
                        ),
                        SizedBox(height: 10.0),
                        AppText(
                          // text:
                          //     'You must go for a travel. Traveling helps get rid of pressure. Go to the mountain to see the nature.',
                          text: detail.place.title,
                          color: AppColors.mainTextColor,
                        ),
                      ],
                    ),
                  ),
                ),

                Positioned(
                  left: 20.0,
                  right: 20.0,
                  bottom: 20.0,
                  child: Row(
                    children: [
                      AppButtons(
                        size: 60.0,
                        icon: Icons.favorite_border_rounded,
                        color: AppColors.textColor2,
                        backgroundColor: Colors.white,
                        borderColor: AppColors.textColor2,
                        isIcon: true,
                      ),
                      SizedBox(width: 20.0),
                      ResponsiveButton(
                        isResponsive: true,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
