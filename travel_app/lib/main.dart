import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_app/logic/app_cubit_logics.dart';
import 'package:travel_app/logic/cubit/app_cubit.dart';
import 'package:travel_app/screens/detail_screen.dart';
import 'package:travel_app/screens/nav_screens/main_screen.dart';
import 'package:travel_app/screens/welcome_screen.dart';
import 'package:travel_app/services/data_services.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Travel App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: BlocProvider<AppCubit>(
        create: (context) => AppCubit(
          data: DataServices(),
        ),
        child: AppCubitLogics(),
      ),
    );
  }
}
