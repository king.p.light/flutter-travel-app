import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:travel_app/models/data_models.dart';

class DataServices {
  String baseUrl = "https://jsonplaceholder.typicode.com/photos";

  Future<List<DataModels>> getInfo() async {
    // var apiUrl = '/getplaces';
    http.Response res = await http.get(Uri.parse(baseUrl));
    try {
      if (res.statusCode == 200) {
        List<dynamic> list = json.decode(res.body);
        print("This is list: $list");
        return list.map((e) => DataModels.fromJson(e)).toList();
      } else {
        return <DataModels>[];
      }
    } catch (error) {
      print((error) => {error.message});
      return <DataModels>[];
    }
  }
}
