part of 'app_cubit.dart';

abstract class AppState extends Equatable {}

class AppInitialState extends AppState {
  @override
  List<Object> get props => [];
}

class AppWelcomeState extends AppState {
  @override
  List<Object> get props => [];
}

class LoadingState extends AppState {
  @override
  List<Object> get props => [];
}

class LoadedState extends AppState {
  LoadedState(this.places);
  final List<DataModels> places;

  @override
  List<Object> get props => [places];
}

class AppDetailState extends AppState {
  AppDetailState(this.place);
  final DataModels place;

  @override
  List<Object> get props => [place];
}
