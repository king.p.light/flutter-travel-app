import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:travel_app/models/data_models.dart';
import 'package:travel_app/services/data_services.dart';

part 'app_state.dart';

class AppCubit extends Cubit<AppState> {
  AppCubit({required this.data}) : super(AppInitialState()) {
    emit(AppWelcomeState());
  }

  final DataServices data;
  late final places;

  void getData() async {
    try {
      emit(LoadingState());
      places = await data.getInfo();
      emit(LoadedState(places));
    } catch (error) {}
  }

  detailScreen(DataModels data) {
    emit(AppDetailState(data));
  }

  goHome() {
    emit(LoadedState(places));
  }
}
